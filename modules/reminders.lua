local reminders = {
	-- {
	--   "%Y", -- (int) four digits year or empty for reminder every year
	--   "%m", -- (int) one or two digits month or empty for reminder every month
	--   "%d", -- REQUIRED, (int) one or two digits day
	--   "*", -- REQUIRED, (string) any text, shows below the calendar
	--   "*" -- (string) any text, empty or nil, can be disabled by beautiful.calendar.show_desc
	--   -- if any REQUIRED entry is empty or nil, table will be ignored
	--   -- if entry is less then minimum (1) or greater then maximum (28/29/30/31) it will be not shown, well, you understand
	-- },
	--- examples:
	-- { "", 4, 26, "remind every 'April, 26th'" },
	-- { 2018, "", 5, "remind 5th day every month only in 2018" },
	-- { "", "", 11, "remind 11th day every month every year" },
	-- { 2017, 7, 20, "a very important meeting" },
	-- { 2017, "07", 19, "", "we have no title, so this reminder will be not shown" },
	-- { 2017, 7, "", "whatever" "we have no day, so this reminder will be not shown" },
	-- { "", os.date("%m"), "8", "remind 8th every month" },
	-- { "", "", (30 - os.date("%d")), "remind at '30 minus today' (someday it will be today, lol)" },
	-- { "", 2*3. 6+8, "remind every 'June, 14th'" },
	-- { "it", "will", "be", "not", "shown or throw an error (depend on lua version)" },
}

return reminders
