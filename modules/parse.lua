local parse = {}

function parse.word_to_line(str)
	if not str then return nil end
	local tbl = {}
	for word in str:gmatch("%w+") do
		table.insert(tbl, word)
	end
	return tbl
end

function parse.only_needed(str, words)
	if type(words) ~= "table" or #words < 1 then return nil end
	local lines = parse.word_to_line(str)
	local tbl = {}
	for _, st in ipairs(lines) do
		for _, rp in ipairs(words) do
			if st == rp then
				tbl[#tbl+1] = st
			end
		end
	end
	return tbl
end

function parse.str_to_tbl(str)
	if not str then return nil end
	local tbl = {}
	local pattern = string.format("([^%s]+)", "\n")
	str:gsub(pattern, function(line) tbl[#tbl+1] = line end)
	return tbl
end

-- based on actionless' (Yauheni Kirylau) code
function parse.tbl_getn(tbl)
	local noi = 0
	for _, _ in pairs(tbl) do
		noi = noi + 1
	end
	return noi
end

-- based on actionless' (Yauheni Kirylau) code
function parse.keyval(str, pat, replaces, post_func)
	local string = parse.str_to_tbl(str)
	local tbl = {}
	local key, val
	local rplcs_len = parse.tbl_getn(replaces)
	for _, line in ipairs(string) do
		if rplcs_len <= 0 then
			-- return tbl
			break
		end
		key, val = line:match(pat)
		for i, o in pairs(replaces) do
			if key == i then
				if post_func then val = post_func(val) end
				tbl[o] = val
				replaces[i] = nil
				rplcs_len = rplcs_len - 1
			end
		end
	end
	return tbl
end

function parse.to_time(sec)
	local s, m, h, d
	local str
	s = math.floor( sec % 60 )
	if string.len(s) == 1 then s = 0 .. s end
	str = s
	m = math.floor( ( sec / 60 ) % 60 )
	-- if m ~= 0 then
		if string.len(m) == 1 then m = 0 .. m end
		str = m .. ":" .. str
	-- end
	h = math.floor( ( sec / 3600 ) % 24 )
	if h ~= 0 then
		if string.len(h) == 1 then h = 0 .. h end
		str = h .. ":" .. str
	end
	d = math.floor( sec / 86400 )
	if d ~= 0 then
		str = d .. "d " .. str
	end
	return str
end

return parse
