local type = type
local ipairs = ipairs
local wibox = require("wibox")
local common = require("awful.widget.common")
local theme = require("beautiful").get()
local cicon = require("modules.cicon")

local upd_func = {}

function upd_func.tags(w, buttons, label, data, objects)
	w:reset()
	for i, o in ipairs(objects) do
		local cache = data[o]
		local ib, tb, bgb, tbm, ibm, l, ct
		if cache then
			ib = cache.ib
			tb = cache.tb
			bgb = cache.bgb
			tbm = cache.tbm
			ibm = cache.ibm
			ct = cache.ct
		else
			ib = wibox.widget.imagebox()
			tb = wibox.widget.textbox()
			bgb = wibox.container.background()
			tbm = wibox.container.margin(tb, 0, 5, 0, 0)
			ibm = wibox.container.margin(ib, 0, 0, 0, 0)
			l = wibox.layout.fixed.horizontal()
			l:fill_space(true)
			l:add(ibm)
			l:add(tbm)
			ct = wibox.container.constraint()
			ct:set_widget(bgb)
			ct:set_strategy("exact")
			ct:set_width(20)
			ct:set_height(24)
			bgb:set_widget(l)
			bgb:buttons(common.create_buttons(buttons, o))
			data[o] = {
				ib = ib,
				tb = tb,
				bgb = bgb,
				tbm = tbm,
				ibm = ibm,
				ct = ct,
			}
		end
		local text, bg, bg_image, icon, args = label(o, tb)
		args = args or {}
		if text == nil or text == "" then
			tbm:set_margins(0)
		else
			if not tb:set_markup_silently(text) then
				tb:set_markup("<i>&lt;Invalid text&gt;</i>")
			end
		end
		bgb:set_bg(bg)
		if type(bg_image) == "function" then
			bg_image = bg_image(tb,o,nil,objects,i)
		end
		bgb:set_bgimage(bg_image)
		if icon then
			ib:set_image(icon)
		else
			ibm:set_margins(0)
		end
		if o.activated and o.selected then
			ibm:set_top(2)
			ibm:set_bottom(-2)
			ibm.opacity = 1
		else
			ibm.opacity = 1
			ibm:set_margins(0)
			if #o:clients() == 0 then
				ibm.opacity = 0.5
			end
		end
		bgb.shape = args.shape
		bgb.shape_border_width = args.shape_border_width
		bgb.shape_border_color = args.shape_border_color
		w:add(ct)
	end
end

---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

function upd_func.tasks(w, buttons, label, data, objects)
	w:reset()
	-- local stack = {}
	for i, o in ipairs(objects) do
		local c = client.focus or {}
		-- if not stack[o.class] then
		--   stack[o.class] = 1
		-- else
		--   stack[o.class] = stack[o.class] + 1
		-- end
		local cache = data[o]
		local ib, tb, bgb, tbm, ibm, l, ct, bi
		if cache then
			ib = cache.ib
			tb = cache.tb
			bgb = cache.bgb
			tbm = cache.tbm
			ibm = cache.ibm
			ct = cache.ct
			-- st = cache.st
		else
			ib = wibox.widget.imagebox()
			tb = wibox.widget.textbox()
			bgb = wibox.container.background()
			bi = wibox.container.background()
			tbm = wibox.container.margin(tb, 0, 1, 0, 0) -- l, r, t, b
			ibm = wibox.container.margin(bi, 4, 12, 3, 3) -- ( 40 - 24 ) / 2
			ct = wibox.container.constraint()
			l = wibox.layout.fixed.horizontal()
			l:fill_space(true)
			l:add(ibm)
			-- l:add(tbm)
			bi:set_widget(ib)
			bi:set_bg(theme.color.normal)
			-- st.valign("center")
			-- st.align("center")
			ct:set_widget(bgb)
			ct:set_strategy("exact")
			ct:set_width(40)
			ct:set_height(30)
			bgb:set_widget(l)
			bgb:buttons(common.create_buttons(buttons, o))
			data[o] = {
				ib = ib,
				tb = tb,
				bgb = bgb,
				tbm = tbm,
				ibm = ibm,
				ct = ct,
				bi = bi
			}
		end
		local text, bg, bg_image, _, args = label(o, tb)
		args = args or {}
		if text == nil or text == "" then
			tbm:set_margins(0)
		else
			if not tb:set_markup_silently(text) then
				tb:set_markup("<i>Untitled</i>")
			end
		end
		bgb:set_bg(bg)
		if type(bg_image) == "function" then
			bg_image = bg_image(tb,o,nil,objects,i)
		end
		bgb:set_bgimage(bg_image)
		ib:set_image(cicon(o))
		ct.opacity = 1.0
		if o.minimized then
			ibm:set_left(-8)
			ibm:set_right(24)
			ct.opacity = 0.5
		elseif o == c then
			ibm:set_left(10)
			ibm:set_right(6)
			-- ct.opacity = 1.0
		else
			ibm:set_left(4)
			ibm:set_right(12)
			-- ct.opacity = 1.0
		end
		bgb.shape = args.shape
		bgb.shape_border_width = args.shape_border_width
		bgb.shape_border_color = args.shape_border_color
		w:add(ct)
	end
end

return upd_func
