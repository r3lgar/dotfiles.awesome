local wibox = require("wibox")
local theme = require("beautiful").get()
local util = require("awful.util")
local button = require("awful.button")
local screen = screen
local mouse = mouse

local calendar = {}
calendar.visible = false

function calendar:hide()
	if self.visible then
		self.widget.visible = false
		self.widget = nil
		self.visible = false
	end
end

function calendar:show(inc_offset)
	self:hide()
	if not self.offset then
		self.offset = 0
	end
	self.day = tonumber(os.date("%d"))
	self.month = tonumber(os.date("%m"))
	self.year = tonumber(os.date("%Y"))
	local reminders = require("modules.reminders")
	local offset = inc_offset or 0
	local padding = theme.calendar.padding or 0
	local month = self.month
	local year = self.year
	local cmd = "cal -m "
	local dt = { "January", "February", "March", "April", "May", "June", "July",
		"August", "September", "October", "November", "December" }
	self.offset = self.offset + offset
	if self.offset > ( 12 - self.month ) + 12 then
		self.offset = ( 12 - self.month ) + 12
	elseif self.offset < -(self.month + 11) then
		self.offset = -(self.month + 11)
	end
	month = month + self.offset
	if month > 12 then
		month = month % 12
		year = year + 1
		if month <= 0 then
			month = 12
		end
	end
	if month < 1 then
		month = month + 12
		year = year - 1
		if month <= 0 then
			month = 1
		end
	end
	local hi = {}
	local rem = ""
	for i in ipairs(reminders) do
		local clr = theme.calendar.rem_color
		if tonumber(reminders[i][1]) == tonumber(year) or reminders[i][1] == "" then
			if tonumber(reminders[i][2]) == tonumber(month) or reminders[i][2] == "" then
				if reminders[i][3] ~= "" then
					if tonumber(reminders[i][3]) == tonumber(self.day) then
						clr = theme.color.mark
					end
					table.insert(hi, reminders[i][3])
					if reminders[i][4] ~= "" then
						rem = rem .. "\n<span color='" .. clr .. "'><b>" .. reminders[i][3] .. "</b> " .. reminders[i][4] .. "</span>"
						if reminders[i][5] and reminders[i][5] ~= "" and theme.calendar.show_desc then
							rem = rem .. "\n" .. reminders[i][5]
						end
					end
				end
			end
		end
	end
	local f = io.popen(cmd .. month .. " " .. year)
	local data = f:read("*all")
		:gsub("[%s]+$", "") -- Crop empty lines
		:gsub("[%s]+\n", "\n") -- Crop spaces at end on lines
		:gsub("^(%s+%w+%s+%d+%s+)", "") -- Crop original header

	f:close()
	if year == self.year and month == self.month then
		data = tostring(data)
		:gsub(
			"([^%S^]+)("
				.. os.date("%e") -- Today with space at start
				.. ")([%s$]+)",
			"%1<span color='"
				.. theme.color.mark -- Or other color to mark today
				.. "' font='bold'>%2</span>%3"
		)
	end
	for i in ipairs(hi) do
		data = data:gsub(
			"([^%S^]+)("
				.. tostring(hi[i]):gsub("^0", "")
				.. ")([%s$]+)",
			"%1<span color='"
				.. theme.calendar.rem_color -- Mark reminds
				.. "' font='bold'>%2</span>%3"
		)
	end
	-- This is fucking piece of crap but I don't know how to align header to center
	local header = dt[month] .. " " .. year
	local hlen = math.floor((20 - string.len(header)) / 2 + 0.5)
	for _ = 1, hlen, 1 do
		header = " " .. header
	end
	data = header .. "\n" .. data
	if rem ~= "" then
		data = data .. "<span font='" .. theme.calendar.rem_font .. "'>" .. rem .. "</span>"
	end
	local cal_txt = wibox.widget.textbox(data)
	cal_txt.font = theme.calendar.font
	cal_txt.wrap = "word"
	local ch = cal_txt:get_height_for_width(120) -- + padding)
	local s_geom = screen[mouse.screen].workarea
	local screen_w = s_geom.x + s_geom.width
	self.widget = wibox({
		visible = true,
		ontop = true,
		x = screen_w - ( 120 + ( padding * 2 )),
		y = s_geom.y,
		width = 120 + ( padding * 2 ),
		height = ch + ( padding * 2 ),
		fg = theme.calendar.fg or "#FFFFFF",
		bg = theme.calendar.bg or "#000000",
		type = "dock"
	})
	self.widget:buttons(util.table.join(
		button({ }, 4, function () self:show(-1) end),
		button({ }, 5, function () self:show(1) end)
	))
	self.widget:setup {
		layout = wibox.container.margin,
		margins = padding,
		{
			layout = wibox.container.constraint,
			strategy = "exact",
			width = 120,
			cal_txt,
		}
	}
	self.visible = true
end

function calendar:toggle(inc_offset)
	self.offset = 0
	if self.visible then
		self:hide()
	else
		self:show(inc_offset)
	end
end

return calendar
