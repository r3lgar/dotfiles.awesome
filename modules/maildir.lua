local timer = require("gears.timer").start_new
local util = require("awful.util")
local imagebox = require("wibox.widget.imagebox")
local theme = require("beautiful").get()
local tooltip = require("awful.tooltip")

local maildir = { mt ={} }

local function update(dir)
	local f_new = io.popen("find " .. dir .. "/*/new/ -type f | wc -l")
	local f_cur = io.popen("find " .. dir .. "/*/cur/ -type f | wc -l")
	local new = f_new:read("*all"):gsub("\n", "")
	local cur = f_cur:read("*all"):gsub("\n", "")
	f_new:close()
	f_cur:close()
	return new, cur
end

local function worker(dir, timeout)
	local icon_new = theme.mail.new
	local icon_no = theme.mail.no
	local i
	local w = imagebox(icon_no, false)
	w._tooltip = tooltip {
		objects = { w },
		delay_show = 0,
		margin_leftright = 8,
		margin_topbottom = 3,
	}
	local t = timer(timeout, function()
		local n, c = update(dir)
		if tonumber(n) > 0 then
			i = icon_new
		else
			i = icon_no
		end
		w.image = i
		w._tooltip:set_text(n .. "/" .. c)
		return true
	end)
	t:again()
	t:emit_signal("timeout")
	return w
end

function maildir.new(args)
	if args == nil or type(args) ~= "table" then return nil end
	if not util.is_dir(args.dir) then return nil end
	if args.timeout == nil or args.timeout <= 0 then args.timeout = (5*60) end
	return worker(args.dir, args.timeout)
end

function maildir.mt:__call(...)
	return maildir.new(...)
end

return setmetatable(maildir, maildir.mt)
