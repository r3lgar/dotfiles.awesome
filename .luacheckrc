-- I get this config from Yauheni Kirylau

-- Only allow symbols available in all Lua versions
std = "min"

-- Get rid of "unused argument self"-warnings
self = false

-- The unit tests can use busted
files["spec"].std = "+busted"

-- The default config may set global variables
files["awesomerc.lua"].allow_defined_top = true

-- Global objects defined by the C code
read_globals = {
    "awesome",
    "button",
    "client",
    "dbus",
    "drawable",
    "drawin",
    "key",
    "keygrabber",
    "mouse",
    "mousegrabber",
    "root",
    "screen",
    "selection",
    "tag",
    "window",
    "jit",
}
-- May not be read-only due to client.focus
globals = {
    "client",
    "localstorage",
    "nlog",
    "log",
    "context"
}

-- vim:ft=lua
